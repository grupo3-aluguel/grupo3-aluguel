package grupo3aluguel.Aluguel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import grupo3aluguel.Aluguel.Aluguel.StatusAluguel;
import grupo3aluguel.Aluguel.Aluguel.StatusCobranca;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class AluguelDAO {
	private ArrayList<Aluguel> alugueis = new ArrayList<>();
	private static AluguelDAO aluguelDAO = null;
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");


	private AluguelDAO() {
		
	}
	
	public static AluguelDAO instance() {
		if (aluguelDAO == null) {
			aluguelDAO = new AluguelDAO();
		}
		return aluguelDAO;
	}
	
	// get all ciclistas
	public ArrayList<Aluguel> getAllAluguel() {
		
		return alugueis;
	}
	
	public Optional<Aluguel> findAluguelByIdCiclistaDevolucao(String id) {
		return alugueis.stream().filter(x -> x.getCiclista().compareTo(id) == 0 && x.getStatusAluguel() == StatusAluguel.ABERTO).findFirst();
	}
	
	public Optional<Aluguel> findAluguelByIdCiclistaAluguel(String id) {
		return alugueis.stream().filter(x -> x.getCiclista().compareTo(id) == 0 && x.getStatusCobranca() == StatusCobranca.PAGAMENTOPENDENTE).findFirst();
	}
	

	// post aluguel
	public Aluguel postAluguel(Aluguel aluguel) {
			
		String horaInicio = dateFormat.format(new Date());
		
		Aluguel createdAluguel = new Aluguel(aluguel.getCiclista(), horaInicio, aluguel.getTrancaInicio());
		alugueis.add(createdAluguel);
		
		return createdAluguel;
		
	}	
	
	public Aluguel postDevolucao(String idCiclista, String idTranca) {
		
        Optional<Aluguel> procuraAluguel = findAluguelByIdCiclistaDevolucao(idCiclista);
        Aluguel aluguelToUpdate = null;
        int cobranca = 0;
        
        if(procuraAluguel.isPresent()) {
        	aluguelToUpdate = procuraAluguel.get();      
        	
        	String horaFim = dateFormat.format(new Date());
        	
        	cobranca = calcularCobranca(horaFim, aluguelToUpdate.getHoraInicio());
        	        	
        	aluguelToUpdate.setHoraFim(horaFim);
        	aluguelToUpdate.setTrancaFim(idTranca);
        	aluguelToUpdate.setCobranca(String.valueOf(cobranca*5));		
		
        }
        
    	return aluguelToUpdate;		

	}

	private int calcularCobranca(String horaFim, String horaInicio) {
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

		LocalDateTime horaInicioDate= LocalDateTime.parse(horaFim, formatter);
    	LocalDateTime horaFimDate= LocalDateTime.parse(horaInicio, formatter);
    	
    	long diferenca = Duration.between(horaFimDate, horaInicioDate).toMinutes();
    	
    	int valorInicial = (int) (diferenca - 120);     
    	
    	if(valorInicial <= 0) { return 0; } 
    	else {
    		if(valorInicial % 30 < 30) { return valorInicial/30 + 1; } 
    		else { return valorInicial/30; }
    	}

	}
}