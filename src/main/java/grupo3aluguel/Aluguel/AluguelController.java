package grupo3aluguel.Aluguel;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

import grupo3aluguel.Aluguel.Aluguel.StatusAluguel;
import grupo3aluguel.Aluguel.Aluguel.StatusCobranca;
import grupo3aluguel.ciclista.Ciclista;
import grupo3aluguel.ciclista.CiclistaDAO;
import io.javalin.http.Handler;
import kong.unirest.*;
import kong.unirest.json.JSONObject;

public class AluguelController {
	
	private static AluguelDAO dao = AluguelDAO.instance();
	
	public static Handler getAllAluguel = ctx -> {
		try {
			ctx.json(dao.getAllAluguel());
			ctx.status(200);

		} catch (Exception e) {
			ctx.html(e.getMessage());

		}
	};
	
	private static String getEmailCiclista(String ciclistaId) {
		CiclistaDAO daoCiclista = CiclistaDAO.instance();

		String email = null;

		Optional<Ciclista> ciclista = daoCiclista.findCiclistaById(ciclistaId);
	
		if(ciclista.isPresent()) {
			email = ciclista.get().getEmail();
		}
		
		//email = "najuborgess@gmail.com";
		
		return email;
			
	}
		
	public static Handler postAluguel = ctx -> {

		Aluguel newAluguel = ctx.bodyAsClass(Aluguel.class);
		String mensagem = "";

		try {
					
			Optional<Aluguel> aluguelPendente = dao.findAluguelByIdCiclistaAluguel(newAluguel.getCiclista());	

			if(aluguelPendente.isPresent()) {
				ctx.json(aluguelPendente.get()).status(200);
			}
			
			else {
								
				Aluguel aluguel = dao.postAluguel(newAluguel);
		
				JSONObject response = fazerCobranca(aluguel.getCiclista(), 10);
				String statusPagamento = response.getString("status");
								
				JsonNode trancaInicio = verificarTranca(aluguel.getTrancaInicio()).getBody();

				if(trancaInicio == null) {
					
					ctx.json("Tranca nao encontrada").status(404);
				}
				else if(trancaInicio.getObject().getString("status").equals("OCUPADA")){
					
					JsonNode bicicleta = verificarBicicletaTranca(trancaInicio.getObject().getString("id")).getBody();

					if(bicicleta.getObject().getString("status").equals("EM REPARO")) {
						
						ctx.json("Ha um reparo requisitado para a bicicleta e ela nao pode ser alugada").status(422);
					}
								
				//verificar Pagamento
				if(statusPagamento.equals("PAGA")) {

					String idBicicleta = bicicleta.getObject().getString("id");
					String idTranca = trancaInicio.getObject().getString("id");

					aluguel.setStatusCobranca(StatusCobranca.FINALIZADO);
					aluguel.setCobranca(null);
					aluguel.setBicicleta(idBicicleta);
										
					atualizarStatusBicicleta(idBicicleta, "EM USO");
					atualizarStatusTranca(idTranca, "DISPONIVEL");


					mensagem = "Pagamento realizado com sucesso."
							+ "Aqui estao os dados do seu aluguel."
							+ " Bicicleta: " + aluguel.getBicicleta() 
							+ "; Tranca: " + aluguel.getTrancaInicio()
							+ "; Hora Inicio: " + aluguel.getHoraInicio();
				}
				else {

					mensagem = "Nao foi possivel concluir seu aluguel."
							+ "Tivemos um problema com seu pagamento";
				}
					
				enviarEmail(getEmailCiclista(aluguel.getCiclista()), mensagem);
				
				ctx.json(aluguel).status(200);
			}
				else {

					ctx.json("Nao ha uma bicicleta nesta tranca").status(422);
				}
			}
		} 
		
		catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getLocalizedMessage());
		}
	};
	
	public static Handler postDevolucao = ctx -> {

		String idCiclista = Objects.requireNonNull(ctx.pathParam("idCiclista"));
		Aluguel newAluguel = Objects.requireNonNull(ctx.bodyAsClass(Aluguel.class));
		String mensagem = "";

		try {
			Optional<Aluguel> getAluguel = dao.findAluguelByIdCiclistaDevolucao(idCiclista);	
			
			if(getAluguel.isPresent()) {
				Aluguel aluguel = dao.postDevolucao(idCiclista, newAluguel.getTrancaFim());
							
				String idBicicleta = aluguel.getBicicleta();
				String idTranca = aluguel.getTrancaFim();
				
				if(verificarBicicleta(idBicicleta).getBody().getObject().getString("status").equals("EM USO")) {
					ctx.json("bicicleta indisponivel").status(422);			
				}
				
				if(verificarTranca(idTranca).getBody().getObject().getString("status").equals("DISPONIVEL")) {
					ctx.json("tranca indisponivel").status(422);			
				}
				
				JSONObject response = fazerCobranca(aluguel.getCiclista(), Integer.parseInt(aluguel.getCobranca()));
				
				String statusPagamento = response.getString("status");
				String idCobranca = response.getString("id");

				if(statusPagamento.equals("PAGA")) {
																			
					aluguel.setStatusCobranca(StatusCobranca.FINALIZADO);
					aluguel.setCobranca(null);
					
					mensagem = "Pagamento realizado com sucesso."
							+ "Aqui estao os dados do seu aluguel."
							+ " Bicicleta: " + aluguel.getBicicleta() 
							+ "; Tranca: " + aluguel.getTrancaFim()
							+ "; Hora Fim: " + aluguel.getHoraFim();
					
				}
				else {
					aluguel.setStatusCobranca(StatusCobranca.PAGAMENTOPENDENTE);
					aluguel.setCobranca(idCobranca);

					mensagem = "Tivemos um problema com seu pagamento."
							+ "Em breve realizaremos uma nova tentativa"
							+ "Aqui estao os dados da devolucao."
							+ " Bicicleta: " + aluguel.getBicicleta() 
							+ "; Tranca: " + aluguel.getTrancaFim()
							+ "; Hora Fim: " + aluguel.getHoraFim();
					
				}
				
				devolverBicicleta(idTranca, idBicicleta);
				atualizarStatusBicicleta(idBicicleta, "DISPONIVEL");
				
				aluguel.setStatusAluguel(StatusAluguel.FINALIZADO);
				enviarEmail(getEmailCiclista(aluguel.getCiclista()), mensagem);
													
				ctx.json(aluguel).status(200);			
			}
			
		}
			
		catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage());
		}
	};
	
	private static HttpResponse<JsonNode> verificarTranca(String idTranca) {
		return Unirest.get("https://grupo2equipamento.herokuapp.com/tranca/{idTranca}")
				.routeParam("idTranca", idTranca)
				.asJson();
	}
	
	private static HttpResponse<JsonNode> verificarBicicletaTranca(String idTranca) {
		return Unirest.get("https://grupo2equipamento.herokuapp.com/tranca/{idTranca}/bicicleta")
				.routeParam("idTranca", idTranca)
				.asJson();
	}
	
	private static HttpResponse<JsonNode> verificarBicicleta(String idBicicleta) {
		return Unirest.get("https://grupo2equipamento.herokuapp.com/bicicleta/{idBicicleta}")
				.routeParam("idBicicleta", idBicicleta)
				.asJson();
	}
	
	private static HttpResponse<JsonNode> atualizarStatusBicicleta(String idBicicleta, String status) {
		String jsonString = "{" + '"' + "status" + '"' + ":" + '"' + status +'"'+"}";
		
		return Unirest.patch("https://grupo2equipamento.herokuapp.com/bicicleta/{idBicicleta}/status")
				.routeParam("idBicicleta", idBicicleta)
				.body(jsonString)
				.asJson();
	}
	
	private static HttpResponse<JsonNode> devolverBicicleta(String idTranca, String idBicicleta){
		String jsonString = "{" + '"' + "idTranca" + '"' + ":" + '"' + idTranca +'"'+ "," 
								+ '"' + "idBicicleta" + '"' + ":" + '"' + idBicicleta +'"' + "}";
		System.out.println(jsonString);
		
		return Unirest.post("https://grupo2equipamento.herokuapp.com/bicicleta/devolucao")
				.body(jsonString)
				.asJson();
	}

	
	private static HttpResponse<JsonNode> atualizarStatusTranca(String idTranca, String status) {
		String jsonString = "{" + '"' + "status" + '"' + ":" + '"' + status +'"'+"}";
		System.out.println(jsonString);
		
		return Unirest.patch("https://grupo2equipamento.herokuapp.com/tranca/{idTranca}/status")
				.routeParam("idTranca", idTranca)
				.body(jsonString)
				.asJson();
	}
	
	private static HttpResponse<JsonNode> enviarEmail(String email,String mensagem) {
		return Unirest.post("https://microsservicoexternobicicleta.herokuapp.com/enviarEmail")
				.queryString("email", email)
				.queryString("mensagem", mensagem)
				.asJson();
	}
	
	private static JSONObject fazerCobranca(String ciclista, int valor) {
		return Unirest.post("https://microsservicoexternobicicleta.herokuapp.com/cobranca")
				.queryString("ciclista", ciclista)
				.queryString("valor", valor)
				.asJson()
				.getBody()
				.getObject();
	}
}
