package grupo3aluguel.Aluguel;

import java.util.UUID;

public class Aluguel {
	
	public enum StatusCobranca{
		FINALIZADO,
		PAGAMENTOPENDENTE,
		ABERTO
	}
	
	public enum StatusAluguel{
		FINALIZADO,
		ABERTO
	}
	
	private String id;
	private String ciclista;
	private String bicicleta;
	private String horaInicio;
	private String trancaInicio;
	private String cobranca;
	private String horaFim;
	private String trancaFim;
	private StatusCobranca statusCobranca;
	private StatusAluguel statusAluguel;
	
	public Aluguel () {
		
	}
	
	public Aluguel(String ciclista, String horaInicio, String trancaInicio) {
		this.id = UUID.randomUUID().toString();
		this.ciclista = ciclista;
		this.horaInicio = horaInicio;
		this.trancaInicio = trancaInicio;
		this.setStatusCobranca(StatusCobranca.ABERTO);
		this.setStatusAluguel(StatusAluguel.ABERTO);
	}
	
	public Aluguel(String ciclista, String horaInicio, String trancaInicio, String bicicleta) {
		this.id = UUID.randomUUID().toString();
		this.ciclista = ciclista;
		this.horaInicio = horaInicio;
		this.trancaInicio = trancaInicio;
		this.setStatusCobranca(StatusCobranca.ABERTO);
		this.setStatusAluguel(StatusAluguel.ABERTO);
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCiclista() {
		return ciclista;
	}
	
	public void setCiclista(String ciclista) {
		this.ciclista = ciclista;
	}
	
	public String getBicicleta() {
		return bicicleta;
	}
	
	public void setBicicleta(String bicicleta) {
		this.bicicleta = bicicleta;
	}
	
	public String getHoraInicio() {
		return horaInicio;
	}
	
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	
	public String getTrancaInicio() {
		return trancaInicio;
	}
	
	public void setTrancaInicio(String trancaInicio) {
		this.trancaInicio = trancaInicio;
	}
	
	public String getCobranca() {
		return cobranca;
	}
	
	public void setCobranca(String cobranca) {
		this.cobranca = cobranca;
	}
	
	public String getHoraFim() {
		return horaFim;
	}
	
	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}
	
	public String getTrancaFim() {
		return trancaFim;
	}
	
	public void setTrancaFim(String trancaFim) {
		this.trancaFim = trancaFim;
	}

	public StatusCobranca getStatusCobranca() {
		return statusCobranca;
	}

	public void setStatusCobranca(StatusCobranca statusCobranca) {
		this.statusCobranca = statusCobranca;
	}

	public StatusAluguel getStatusAluguel() {
		return statusAluguel;
	}

	public void setStatusAluguel(StatusAluguel statusAluguel) {
		this.statusAluguel = statusAluguel;
	}

	
}
