package grupo3aluguel.meiopagamento;
import java.util.Objects;
import java.util.logging.*;

import grupo3aluguel.ciclista.Ciclista;
import grupo3aluguel.ciclista.CiclistaDAO;

import java.util.NoSuchElementException;
import io.javalin.http.Handler;

public class MeioPagamentoController {
	
	static Logger logger = Logger.getLogger(MeioPagamentoController.class.getName());
	static CiclistaDAO daoCiclista = CiclistaDAO.instance();

	
	public static Handler getAllCartoes = ctx -> {
		MeioPagamentoDAO dao = MeioPagamentoDAO.instance();
		try {
			ctx.json(dao.findAllCartoes());
			ctx.status(200);

		} catch (Exception e) {
			logger.log(Level.INFO, "Msg", e);
			ctx.html(e.getMessage());

		}
	};
	
	public static Handler getMeioPagamentoById = ctx -> {
		
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		//MeioPagamentoDAO dao = MeioPagamentoDAO.instance();

		try {
			Ciclista ciclista = daoCiclista.findCiclistaById(id).get();
			//MeioPagamento meioPagamento = dao.findMeioPagamentoById(id).get();

				ctx.json(ciclista.getMeioDePagamento()).status(200);					

		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
		}

	};


	public static Handler postMeioPagamento = ctx -> {

		MeioPagamentoDAO dao = MeioPagamentoDAO.instance();
		MeioPagamento newMeioPagamento = Objects.requireNonNull(ctx.bodyAsClass(MeioPagamento.class));

		try {

			// validate if exists
			boolean alreadyExists = dao.findMeioPagamentoById(newMeioPagamento.getId()).isPresent();
			if (alreadyExists == false) {
				MeioPagamento meioPagamento = dao.postMeioPagamento(newMeioPagamento);
				ctx.json(meioPagamento).status(200);

			} else {
				ctx.status(400).json("Already Exists");
			}

		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
			logger.log(Level.INFO, "Msg", e);
		}
	};

	public static Handler updateMeioPagamento = ctx -> {

		MeioPagamentoDAO dao = MeioPagamentoDAO.instance();
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		MeioPagamento newMeioPagamento = Objects.requireNonNull(ctx.bodyAsClass(MeioPagamento.class));

		try {
			Ciclista ciclista = daoCiclista.findCiclistaById(id).get();

			// validate if exists
			MeioPagamento meioPagamento = dao.updateMeioPagamento(ciclista.getMeioDePagamento().getId(), newMeioPagamento.getNomeTitular(), newMeioPagamento.getNumero(), newMeioPagamento.getValidade(), newMeioPagamento.getCvv());
			ciclista.setMeioDePagamento(meioPagamento);
			
			ctx.json(ciclista).status(200);
		
		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
			logger.log(Level.INFO, "Msg", e);
		}
	};


}
