package grupo3aluguel.meiopagamento;

import java.util.*;

import grupo3aluguel.ciclista.Ciclista;

public class MeioPagamentoDAO {
	public MeioPagamento meioPagamento = new MeioPagamento("Anna Bia", "12345678910", "25/09/2025", "123");
	public MeioPagamento meioPagamento2 = new MeioPagamento("Anna Maria", "12345678910", "28/06/2021", "321");
	public MeioPagamento meioPagamento3 = new MeioPagamento("Mariana", "12345678910", "25/09/2025", "123");

	private ArrayList<MeioPagamento> meiosDePagamento = new ArrayList<>();

	private static MeioPagamentoDAO meioDePagamentoDAO = null;

	private MeioPagamentoDAO() {
		meiosDePagamento.add(meioPagamento);
		meiosDePagamento.add(meioPagamento2);
		meiosDePagamento.add(meioPagamento3);
	}
	
	public ArrayList<MeioPagamento> findAllCartoes() {
		
		return meiosDePagamento;
	}
	
	public static MeioPagamentoDAO instance() {
		if (meioDePagamentoDAO == null) {
			meioDePagamentoDAO = new MeioPagamentoDAO();
		}
		return meioDePagamentoDAO;
	}
	
	public void resetInstance() {
		meioDePagamentoDAO = null;
	}

	// get meioDePagamento by id
	public Optional<MeioPagamento> findMeioPagamentoById(String id) {

		return meiosDePagamento.stream().filter(x -> x.getId().compareTo(id) == 0).findFirst();

	}
	
	public Optional<MeioPagamento> findMeioPagamentoByNomeTitular(String nome) {
		return meiosDePagamento.stream().filter(x -> x.getNomeTitular().compareTo(nome) == 0).findFirst();

	}
	
	// post meioDePagamento
	public MeioPagamento postMeioPagamento(MeioPagamento meioPagamento) {

		MeioPagamento createdMeioPagamento = new MeioPagamento(meioPagamento.getNomeTitular(), meioPagamento.getNumero(), meioPagamento.getValidade(), meioPagamento.getCvv());
		meiosDePagamento.add(createdMeioPagamento);
		return createdMeioPagamento;
		
	}

	// update meioDePagamento
	public MeioPagamento updateMeioPagamento(String meioDePagamentoId, String nomeTitular, String numero, String validade, String cvv) {
		Optional <MeioPagamento> procuraMeioPagamento = findMeioPagamentoById(meioDePagamentoId);
		MeioPagamento meioDePagamentoToUpdate = null;
		if( procuraMeioPagamento.isPresent()) {
			meioDePagamentoToUpdate = procuraMeioPagamento.get();
			meioDePagamentoToUpdate.setNomeTitular(nomeTitular);
			meioDePagamentoToUpdate.setNumero(numero);
			meioDePagamentoToUpdate.setValidade(validade);
			meioDePagamentoToUpdate.setCvv(cvv);
		}
		
		return meioDePagamentoToUpdate;
	}

	
}
