package grupo3aluguel.funcionario;
import java.util.UUID;

public class Funcionario {
	private String id;
	private String matricula;
	private String senha;
	private String nome;
	private int idade;
	private String funcao;
	private String cpf;
	private String email;
	
	public Funcionario()
    {
		
    }
	
	public Funcionario(String matricula , String senha, String nome, int idade, String funcao, String cpf, String email) {
        this.id = UUID.randomUUID().toString();
		this.matricula = matricula;
		this.senha = senha;
		this.nome = nome;
		this.idade = idade;
		this.funcao = funcao;
		this.cpf = cpf;
		this.email = email;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
