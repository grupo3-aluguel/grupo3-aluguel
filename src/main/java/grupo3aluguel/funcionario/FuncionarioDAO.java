package grupo3aluguel.funcionario;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class FuncionarioDAO {
	private ArrayList<Funcionario> funcionarios = new ArrayList<>();
	private Funcionario funcionario = new Funcionario("123456", "xxxxx", "Mario", 32, "mecanico", "12345678910", "mario@gmail.com");
	private Funcionario funcionario2 = new Funcionario("654321", "xxxxx", "pietra dos santos", 25, "tecnica", "12345678910", "pietra@gmail.com");
	private Funcionario funcionario3 = new Funcionario("142536", "xxxxx", "Marcio da Fonseca", 45, "mecanico", "12345678910", "Marcio@gmail.com");

	private static FuncionarioDAO funcionarioDAO = null;

	private FuncionarioDAO() {
		funcionarios.add(funcionario);
		funcionarios.add(funcionario2);
		funcionarios.add(funcionario3);
	}
	
	public static FuncionarioDAO instance() {
		if (funcionarioDAO == null) {
			funcionarioDAO = new FuncionarioDAO();
		}
		return funcionarioDAO;
	}
	
	public void resetInstance() {
		funcionarioDAO = null;
	}

	// get all funcionarios
	public ArrayList<Funcionario> findAllFuncionarios() {

		return funcionarios;

	}

	// get funcionario by id
	public Optional<Funcionario> findFuncionarioById(String id) {

		return funcionarios.stream().filter(x -> x.getId().compareTo(id) == 0).findFirst();

	}
	
	public Optional<Funcionario> findFuncionarioByNome(String nome) {

		return funcionarios.stream().filter(x -> x.getId().compareTo(nome) == 0).findFirst();

	}

	// post funcionario
	public Funcionario postFuncionario(Funcionario funcionario) {

		Funcionario createdFuncionario = new Funcionario(funcionario.getMatricula(), funcionario.getSenha(), funcionario.getNome(), funcionario.getIdade(), funcionario.getFuncao(), funcionario.getCpf(), funcionario.getEmail());
		funcionarios.add(createdFuncionario);
		return createdFuncionario;

	}

	// update funcionario
	public Funcionario updateFuncionario(String funcionarioId, String nome, int idade, String funcao, String email) {
		Optional<Funcionario> procuraFuncionario = findFuncionarioById(funcionarioId);
		Funcionario funcionarioToUpdate = null;
		if(procuraFuncionario.isPresent()) {
			funcionarioToUpdate = procuraFuncionario.get();
			funcionarioToUpdate.setNome(nome);
			funcionarioToUpdate.setIdade(idade);
			funcionarioToUpdate.setFuncao(funcao);
			funcionarioToUpdate.setEmail(email);
		}

		return funcionarioToUpdate;
	}

	// delete funcionario
	public void deleteFuncionario(String id) {

		funcionarios.removeIf(x -> x.getId().compareTo(id) == 0);

	}


}
