package grupo3aluguel.funcionario;

import java.util.Objects;

import java.util.NoSuchElementException;
import io.javalin.http.Handler;
import java.util.logging.*;


public class FuncionarioController {
	
	static Logger logger = Logger.getLogger(FuncionarioController.class.getName());
	
	public static Handler getAllFuncionarios = ctx -> {
		FuncionarioDAO dao = FuncionarioDAO.instance();
		try {
			ctx.json(dao.findAllFuncionarios());
			ctx.status(200);

		} catch (Exception e) {
			ctx.json(e).status(500);
		}
	};

	public static Handler getFuncionarioById = ctx -> {
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		FuncionarioDAO dao = FuncionarioDAO.instance();

		try {
			Funcionario funcionario = dao.findFuncionarioById(id).get();

			if (funcionario == null) {
				ctx.status(404).json("Not Found");

			} else {
				ctx.json(funcionario).status(200);
			}

		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
		}

	};

	public static Handler postFuncionario = ctx -> {

		FuncionarioDAO dao = FuncionarioDAO.instance();
		Funcionario newFuncionario = ctx.bodyAsClass(Funcionario.class);
		
		try {
			
			Funcionario funcionario = dao.postFuncionario(newFuncionario);
			ctx.json(funcionario).status(201);

		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
			logger.log(Level.INFO, "Msg", e);
		}
	};

	public static Handler updateFuncionario = ctx -> {

		FuncionarioDAO dao = FuncionarioDAO.instance();
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		Funcionario newFuncionario = Objects.requireNonNull(ctx.bodyAsClass(Funcionario.class));

		try {

			// validate if exists
			Funcionario funcionarioExists = dao.findFuncionarioById(id).get();
			
			if(funcionarioExists == null) {
				ctx.json("Nao encontrado").status(404);
			}
			
			if (funcionarioExists != null) {
				Funcionario funcionario = dao.updateFuncionario(id, newFuncionario.getNome(), newFuncionario.getIdade(), newFuncionario.getFuncao(), newFuncionario.getEmail());
				ctx.json(funcionario).status(200);

			}
		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
			logger.log(Level.INFO, "Msg", e);
		}
	};

	public static Handler deleteFuncionario = ctx -> {

		FuncionarioDAO dao = FuncionarioDAO.instance();
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		try {

			// validate if exists
			Funcionario funcionarioExists = dao.findFuncionarioById(id).get();
			if (funcionarioExists != null) {
				dao.deleteFuncionario(id);
				ctx.json("Funcionario com id: " + id + " deletado").status(200);

			}
		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
			logger.log(Level.INFO, "Msg", e);
		}

	};


}
