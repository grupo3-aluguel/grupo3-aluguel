package grupo3aluguel.ciclista;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import grupo3aluguel.ciclista.Ciclista.Nacionalidade;
import grupo3aluguel.ciclista.Ciclista.Status;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;


@JsonAutoDetect(fieldVisibility = Visibility.ANY)
public class CiclistaDAO {
	private ArrayList<Ciclista> ciclistas = new ArrayList<>();
			
	private Ciclista ciclista = new Ciclista("Anna Bia", "05/01/1995", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.bia@gmail.com", MeioPagamentoDAO.instance().meioPagamento);
	private Ciclista ciclista1 = new Ciclista("Anna Maria", "08/05/1986", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.maria@gmail.com", MeioPagamentoDAO.instance().meioPagamento2);
	private Ciclista ciclista2 = new Ciclista("Mariana", "15/10/2000", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "mariana@gmail.com", MeioPagamentoDAO.instance().meioPagamento3);

	private static CiclistaDAO ciclistaDAO = null;

	public CiclistaDAO() {
		ciclistas.add(ciclista);
		ciclistas.add(ciclista1);
		ciclistas.add(ciclista2);

	}
	
	public static CiclistaDAO instance() {
		if (ciclistaDAO == null) {
			ciclistaDAO = new CiclistaDAO();
		}
		return ciclistaDAO;
	}
	
	public void resetInstance() {
		ciclistaDAO = null;
	}
	
	// get all ciclistas
	public ArrayList<Ciclista> findAllCiclistas() {
		
		return ciclistas;
	}
	
	// get ciclista by id
	public Optional<Ciclista> findCiclistaById(String id) {
		return ciclistas.stream().filter(x -> x.getId().compareTo(id) == 0).findFirst();

	}
	
	public Optional<Ciclista> findCiclistaByNome(String nome) {
		return ciclistas.stream().filter(x -> x.getNome().compareTo(nome) == 0).findFirst();

	}

	// post ciclistas
	public Ciclista postCiclista(Ciclista ciclista) {
		Ciclista createdCiclista;
		
		if(ciclista.getNacionalidade() == Nacionalidade.BRASILEIRO) {
			createdCiclista = new Ciclista(ciclista.getNome(), ciclista.getNascimento(), ciclista.getCpf(), ciclista.getNacionalidade(), ciclista.getSenha(), ciclista.getEmail(), ciclista.getMeioDePagamento());
		}
		else {
			createdCiclista = new Ciclista(ciclista.getNome(), ciclista.getNascimento(), ciclista.getPassaporte(), ciclista.getNacionalidade(), ciclista.getSenha(), ciclista.getEmail(), ciclista.getMeioDePagamento());
		}
		
		
		ciclistas.add(createdCiclista);
		return createdCiclista;
		
	}
	
	// update ciclista
	public Ciclista updateCiclista(String id, String nome, String nascimento, String email) {
		Optional<Ciclista> procuraCiclista = findCiclistaById(id);
		Ciclista ciclistaToUpdate = null;
		if(procuraCiclista.isPresent()) {
			ciclistaToUpdate = procuraCiclista.get();
			ciclistaToUpdate.setNome(nome);
			ciclistaToUpdate.setNascimento(nascimento);
			ciclistaToUpdate.setEmail(email);			
		}
		return ciclistaToUpdate;
	}
	
	// ativar ciclista
	public Ciclista ativarCiclista(String id) {
		Optional<Ciclista> procuraCiclista = findCiclistaById(id);
		Ciclista ciclistaToUpdate = null;
		if(procuraCiclista.isPresent()) {
			ciclistaToUpdate = procuraCiclista.get();
			ciclistaToUpdate.setStatus(Status.ATIVO);		
		}
		return ciclistaToUpdate;
	}
	
	// verficar existe email ciclista	
	public boolean verificaExisteEmail(String email) {
		for(Ciclista item : ciclistas) {
			  if(item.getEmail().equals(email)) {
				  return true;
			  }
		}
		return false;
	}





}
