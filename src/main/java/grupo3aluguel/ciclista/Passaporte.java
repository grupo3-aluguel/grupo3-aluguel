package grupo3aluguel.ciclista;

public class Passaporte {
	
	public String numero;
	public String validade;
	public String pais;
	
	public Passaporte()
    {
		
    }
	
	public Passaporte(String numero, String validade, String pais) {
		this.numero = numero;
		this.pais = pais;
		this.validade = validade;
	}

}
