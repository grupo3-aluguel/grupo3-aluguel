package grupo3aluguel.ciclista;

import java.util.Objects;
import java.util.NoSuchElementException;
import io.javalin.http.Handler;
import java.util.logging.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import grupo3aluguel.meiopagamento.MeioPagamento;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;


public class CiclistaController {
	
	static Logger logger = Logger.getLogger(CiclistaController.class.getName());
	
	public static Handler getAllCiclistas = ctx -> {
		CiclistaDAO dao = CiclistaDAO.instance();
		try {
			ctx.json(dao.findAllCiclistas());
			ctx.status(200);

		} catch (Exception e) {
			logger.log(Level.INFO, "Msg", e);
			ctx.json(e.getMessage());

		}
	};

	public static Handler getCiclistaById = ctx -> {
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		CiclistaDAO dao = CiclistaDAO.instance();

		try {
			Ciclista ciclista = dao.findCiclistaById(id).get();

			ctx.json(ciclista).status(200);
			

		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
		}

	};
	
	public static boolean verificaEmail(String email) {
		final String EMAIL_PATTERN = 
		        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		final Pattern pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
		
		Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	}

	public static Handler postCiclista = ctx -> {

		CiclistaDAO dao = CiclistaDAO.instance();
		Ciclista newCiclista = ctx.bodyAsClass(Ciclista.class);

		try {
			
			if(verificaEmail(newCiclista.getEmail())){
				
				MeioPagamento meioDePagamento = new MeioPagamento(newCiclista.getMeioDePagamento().getNomeTitular(), newCiclista.getMeioDePagamento().getNumero(), newCiclista.getMeioDePagamento().getValidade(), newCiclista.getMeioDePagamento().getCvv());
				MeioPagamentoDAO.instance().postMeioPagamento(meioDePagamento);
				newCiclista.setMeioDePagamento(meioDePagamento);
				
				Ciclista ciclista = dao.postCiclista(newCiclista);
				ctx.json(ciclista).status(201);
			}
			
			else {
				ctx.json("email " + newCiclista.getEmail() + " invalido").status(405);
			}

		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
			logger.log(Level.INFO, "Msg", e);
		}
	};

	public static Handler updateCiclista = ctx -> {

		CiclistaDAO dao = CiclistaDAO.instance();
		String id = Objects.requireNonNull(ctx.pathParam("id"));
		Ciclista newCiclista = Objects.requireNonNull(ctx.bodyAsClass(Ciclista.class));

		try {
			
			Ciclista ciclistaExists = dao.findCiclistaById(id).get();

			if(verificaEmail(newCiclista.getEmail()) && ciclistaExists != null){
					Ciclista ciclista = dao.updateCiclista(id, newCiclista.getNome(), newCiclista.getNascimento(), newCiclista.getEmail());
					ctx.json(ciclista).status(200);
			}
			else {
				ctx.json("email " + newCiclista.getEmail() + " invalido");
			}
			
		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
		}
	};
	
	public static Handler ativarCiclista = ctx -> {

		CiclistaDAO dao = CiclistaDAO.instance();
		String id = Objects.requireNonNull(ctx.pathParam("id"));

		try {
			
			Ciclista ciclistaExists = dao.findCiclistaById(id).get();

			// validate if exists
			if (ciclistaExists != null) {
				Ciclista ciclista = dao.ativarCiclista(id);
				ctx.json(ciclista).status(200);
			}
			
		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		}catch (Exception e) {
			ctx.json(e.getMessage()).status(500);
		}
	};
	
	public static Handler verificaExisteEmail = ctx -> {
		CiclistaDAO dao = CiclistaDAO.instance();
		String email = Objects.requireNonNull(ctx.pathParam("email"));
		
		if(email == null) {
			ctx.json("Email nao enviado como parametro").status(400);
		}
		
		try {
			boolean existEmail = dao.verificaExisteEmail(email);
	
				ctx.json(existEmail).status(200);

		} catch (IllegalArgumentException e) {
			ctx.json("Invalid Data").status(422);
		} catch (NoSuchElementException e) {
			ctx.status(404).json("Not Found");
		} catch(Exception e){
			ctx.json(e.getMessage()).status(500);
		}
		
	};


}
