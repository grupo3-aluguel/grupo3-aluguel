package grupo3aluguel.ciclista;

import grupo3aluguel.meiopagamento.MeioPagamento;
import java.util.UUID;


public class Ciclista {
	
	public enum Nacionalidade{
		BRASILEIRO,
		ESTRANGEIRO
	}
	
	public enum Status{
		ATIVO,
		AGUARDANDO_CONFIRMACAO
	}

	private String id;
	private Status status; 
	private String nome;
	private String nascimento;
	private String cpf;
	private Passaporte passaporte;
	private Nacionalidade nacionalidade;
	private String senha;
	private String email;
	private MeioPagamento meioDePagamento;
	
	public Ciclista()
    {
		
    }
	
	public Ciclista(String id, String nome, String nascimento, String cpf, Nacionalidade nacionalidade, String senha, String email, MeioPagamento meioDePagamento ) {
        this.id = id;
		this.nome = nome;
		this.nascimento = nascimento;
		this.cpf = cpf;
		this.nacionalidade = nacionalidade;
		this.senha = senha;
		this.email = email;
		this.meioDePagamento = meioDePagamento;
		this.status = Status.AGUARDANDO_CONFIRMACAO;
	}
	
	public Ciclista(String nome, String nascimento, String cpf, Nacionalidade nacionalidade, String senha, String email, MeioPagamento meioDePagamento ) {
        this.id = UUID.randomUUID().toString();
		this.nome = nome;
		this.nascimento = nascimento;
		this.cpf = cpf;
		this.nacionalidade = nacionalidade;
		this.senha = senha;
		this.email = email;
		this.meioDePagamento = meioDePagamento;
		this.status = Status.AGUARDANDO_CONFIRMACAO;
	}
	
	public Ciclista(String nome, String nascimento,  Passaporte passaporte, Nacionalidade nacionalidade, String senha, String email, MeioPagamento meioDePagamento) {
        this.id = UUID.randomUUID().toString();
		this.nome = nome;
		this.nascimento = nascimento;
		this.passaporte = passaporte;
		this.nacionalidade = nacionalidade;
		this.senha = senha;
		this.email = email;
		this.meioDePagamento = meioDePagamento;
		this.status = Status.AGUARDANDO_CONFIRMACAO;

	}

	public String getId() {
		return id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNascimento() {
		return nascimento;
	}

	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Passaporte getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(Passaporte passaporte) {
		this.passaporte = passaporte;
	}

	public Nacionalidade getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(Nacionalidade nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public MeioPagamento getMeioDePagamento() {
		return meioDePagamento;
	}

	public void setMeioDePagamento(MeioPagamento meioDePagamento) {
		this.meioDePagamento = meioDePagamento;
	}
	
	 


}

