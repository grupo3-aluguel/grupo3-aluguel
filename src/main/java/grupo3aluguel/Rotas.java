package grupo3aluguel;

import grupo3aluguel.Aluguel.AluguelController;
import grupo3aluguel.ciclista.CiclistaController;
import grupo3aluguel.funcionario.FuncionarioController;
import grupo3aluguel.meiopagamento.MeioPagamentoController;
import io.javalin.Javalin;

public class Rotas {
	
	private Javalin app = Javalin.create(config -> {
	    config.defaultContentType = "application/json";
	 });

	public void startApplication(int port) {
		
		this.app.start(port);
		
		String funcionarioId = "/funcionario/:id";		
		
		app.get("/", ctx -> ctx.result("Hello Heroku"));
		
		app.get("/ciclista", CiclistaController.getAllCiclistas);
		app.post("/ciclista", CiclistaController.postCiclista);
		app.get("/ciclista/:id",  CiclistaController.getCiclistaById);
		app.put("/ciclista/:id",  CiclistaController.updateCiclista);
		app.put("/ciclista/:id/ativar",  CiclistaController.ativarCiclista);
		app.get("ciclista/existeEmail/:email",  CiclistaController.verificaExisteEmail);
		
		app.get("/funcionario", FuncionarioController.getAllFuncionarios);
		app.post("/funcionario", FuncionarioController.postFuncionario);
		app.get(funcionarioId, FuncionarioController.getFuncionarioById);
		app.put(funcionarioId, FuncionarioController.updateFuncionario);
		app.delete(funcionarioId, FuncionarioController.deleteFuncionario);
		
		app.get("/cartaoDeCredito", MeioPagamentoController.getAllCartoes);
		app.get("/cartaoDeCredito/:id", MeioPagamentoController.getMeioPagamentoById);
		app.put("cartaoDeCredito/:id", MeioPagamentoController.updateMeioPagamento);
		
		app.get("/aluguel/", AluguelController.getAllAluguel);
		app.post("/aluguel/", AluguelController.postAluguel);
		app.post("/devolucao/:idCiclista/", AluguelController.postDevolucao);
	}
	
	public void stop() {
		app.stop();
	}

			
}
