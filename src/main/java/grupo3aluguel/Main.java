package grupo3aluguel;

public class Main {

	public static void main(String[] args) {
	    Rotas app = new Rotas();
	    app.startApplication(getHerokuAssignedPort());
	}

	private static int getHerokuAssignedPort() {
	  String herokuPort = System.getenv("PORT");
	  if (herokuPort != null) {
	    return Integer.parseInt(herokuPort);
	  }
	  return 7002;
	}

}