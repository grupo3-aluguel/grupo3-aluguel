import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import grupo3aluguel.funcionario.Funcionario;
import grupo3aluguel.funcionario.FuncionarioDAO;

public class TesteFuncionario {

	FuncionarioDAO dao; 
	Funcionario funcionario = new Funcionario("123456", "xxxxx", "mario castro", 32, "mecanico", "12345678910", "mario@gmail.com");
	String idFuncionario = funcionario.getId();
	
	@Before
	public void before() {
		dao = FuncionarioDAO.instance();
	}
	
	@After
	public void after() {
		dao.resetInstance();
	}
	
	@Test
	public void testGetId() {
		assertEquals(idFuncionario,funcionario.getId());
	}
	
	@Test
	public void testSetId() {
		funcionario.setId("12345");
		assertEquals("12345", funcionario.getId());
	}
	
	@Test
	public void testGetMatricula() {
		assertEquals("123456",funcionario.getMatricula());
	}
	
	@Test
	public void testSetMatricula() {
		funcionario.setMatricula("654321");
		assertEquals("654321",funcionario.getMatricula());
	}
	
	@Test
	public void testGetSenha() {
		assertEquals("xxxxx",funcionario.getSenha());
	}
	
	@Test
	public void testSetSenha() {
		funcionario.setSenha("yyyyy");
		assertEquals("yyyyy",funcionario.getSenha());
	}
	
	@Test
	public void testGetNome() {
		assertEquals("mario castro",funcionario.getNome());
	}
	
	@Test
	public void testSetNome() {
		funcionario.setNome("Mario de Castro");
		assertEquals("Mario de Castro",funcionario.getNome());
	}
	
	@Test
	public void testGetIdade() {
		assertEquals(32,funcionario.getIdade());
	}
	
	@Test
	public void testSetIdade() {
		funcionario.setIdade(34);
		assertEquals(34,funcionario.getIdade());
	}
	
	@Test
	public void testGetFuncao() {
		assertEquals("mecanico",funcionario.getFuncao());
	}
	
	@Test
	public void testSetFuncao() {
		funcionario.setFuncao("Tecnico");
		assertEquals("Tecnico",funcionario.getFuncao());
	}
	
	@Test
	public void testGetCpf() {
		assertEquals("12345678910",funcionario.getCpf());
	}
	
	@Test
	public void testSetCpf() {
		funcionario.setCpf("01987654321");
		assertEquals("01987654321",funcionario.getCpf());
	}

}
