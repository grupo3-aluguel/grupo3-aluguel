import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;

import grupo3aluguel.Rotas;
import grupo3aluguel.ciclista.CiclistaDAO;
import grupo3aluguel.funcionario.Funcionario;
import grupo3aluguel.funcionario.FuncionarioDAO;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class TesteFuncionarioController {
	 public FuncionarioDAO funcionarioDAO;
	 public Rotas app = new Rotas();
	 
	  @Test
	  public void getAllFuncionariosTest() {
		app.startApplication(9000);
		HttpResponse<String> responseponse = Unirest.get("http://localhost:9000/funcionario").asString();
		assertEquals(200, responseponse.getStatus());
		app.stop();
	  }
	  
	  @Test
	  public void getFuncionarioByIdTest() {
		app.startApplication(9001);
		FuncionarioDAO dao = FuncionarioDAO.instance();
		Optional<Funcionario> funcionarioExists = dao.findFuncionarioByNome("Mario");
		
		if (funcionarioExists.isPresent()) {
			Funcionario funcionario = funcionarioExists.get();
			HttpResponse<String> responseponse = Unirest.get("http://localhost:9001/funcionario/" + funcionario.getId()).asString();
			assertEquals(200, responseponse.getStatus());
		}
		
		dao.resetInstance();
		app.stop();
	  }
	  
	  @Test
	  public void getFuncionarioById404Test() {
		  
		app.startApplication(9002);
			
		HttpResponse<JsonNode> responseponse = Unirest.get("http://localhost:9002/funcionario/{idFuncionario}")
				.routeParam("idFuncionario", "1")
				.asJson();
		
		assertEquals(404, responseponse.getStatus());
				
		app.stop();
	  }
	  
	  @Test
	  public void postFuncionarioTest() {
	    app.startApplication(9003);
	    Funcionario funcionario = new Funcionario("654321", "xxxxx", "pietra dos santos", 25, "tecnica", "12345678910", "pietra@gmail.com");
	    String funcionarioToJson = JavalinJson.toJson(funcionario);
	    
	    HttpResponse<String> response = Unirest.post("http://localhost:9003/funcionario").body(funcionarioToJson).asString();
	    assertEquals(201, response.getStatus());
	    app.stop();
	  }

	  @Test
	  public void post400FuncionarioTest() {
	    app.startApplication(9004);
		Funcionario funcionario = null;
	    HttpResponse<String> response = Unirest.post("http://localhost:9004/funcionario").body(funcionario).asString();
	    assertEquals(400, response.getStatus());
	    app.stop();
	  }
	  
	  
	  @Test
	  public void putFuncionarioTest() {
	    app.startApplication(9005);
	    
		FuncionarioDAO dao = FuncionarioDAO.instance();
	    Funcionario funcionario = new Funcionario("654321", "xxxxx", "pietra dos santos", 25, "tecnica", "12345678910", "pietra@gmail.com");
		dao.postFuncionario(funcionario);
		
	    Optional<Funcionario> funcionarioExist = dao.findFuncionarioByNome("teste");
	    
	    if (funcionarioExist.isPresent()) {
	      Funcionario funcionarioToUpdate = funcionarioExist.get();
	      String id = funcionarioToUpdate.getId();
	      String funcionarioToJson = JavalinJson.toJson(funcionarioToUpdate);
	      
	      HttpResponse<String> response = Unirest.put("http://localhost:9005/funcionario/{idFuncionario}")
	    		  .routeParam("idFuncionario", id)
	    		  .body(funcionarioToJson).asString();
	      
	      assertEquals(200, response.getStatus());
	    }
	    
	    dao.resetInstance();
	    app.stop();
	  }

	  @Test
	  public void putFuncionarioInvalidTest() {
	    app.startApplication(9006);
		FuncionarioDAO dao = null;
	    String id = "2";
	    HttpResponse<String> response = Unirest.put("http://localhost:9006/funcionario/{idFuncionario}")
	    		.routeParam("idFuncionario", id)
	    		.body(dao).asString();
	    
	    assertEquals(400, response.getStatus());
	    app.stop();
	  }
	  
	  @Test
	  public void deleteFuncionarioTest() {
	    app.startApplication(9008);
	    FuncionarioDAO dao = FuncionarioDAO.instance();
	    Funcionario funcionario = new Funcionario("654321", "xxxxx", "teste", 25, "tecnica", "12345678910", "pietra@gmail.com");
	    
	    dao.postFuncionario(funcionario);
	    
	    Optional<Funcionario> funcionarioExists = dao.findFuncionarioByNome("teste");
	    
	    if (funcionarioExists.isPresent()) {
	      Funcionario funcionarioDemitido = funcionarioExists.get();

	      HttpResponse<String> res = Unirest.delete("http://localhost:9008/funcionario/{idFuncionario}")
	    		  .routeParam("idFuncionario", funcionarioDemitido.getId())
	    		  .asString();
	      assertEquals(200, res.getStatus());
	    }
	    
	    dao.resetInstance();
	    app.stop();
	  }

	  @Test
	  public void DeleteInvalidFuncionarioTest() {
		  app.startApplication(9009);
		  FuncionarioDAO dao = null;
		    String id = "2";
		    HttpResponse<String> response = Unirest.delete("http://localhost:9009/funcionario/{idFuncionario}")
		    		.routeParam("idFuncionario", id)
		    		.body(dao).asString();
		    
		    assertEquals(404, response.getStatus());
		    app.stop();
	  }



}
