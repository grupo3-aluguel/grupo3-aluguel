import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.*;

import grupo3aluguel.funcionario.Funcionario;
import grupo3aluguel.funcionario.FuncionarioDAO;

public class TesteFuncionarioDao {
	FuncionarioDAO dao; 
	Funcionario funcionario = new Funcionario("123456", "xxxxx", "mario castro", 32, "mecanico", "12345678910", "mario@gmail.com");
	String idFuncionario = funcionario.getId();
	
	@Before
	public void before() {
		dao = FuncionarioDAO.instance();
	}
	
	@After
	public void after() {
		dao.resetInstance();
	}

	@Test
	public void testPostFuncionario() {		
		assertEquals(funcionario.getMatricula(),dao.postFuncionario(funcionario).getMatricula());
		assertEquals(funcionario.getSenha(),dao.postFuncionario(funcionario).getSenha());
		assertEquals(funcionario.getNome(),dao.postFuncionario(funcionario).getNome());
		assertEquals(funcionario.getIdade(),dao.postFuncionario(funcionario).getIdade());
		assertEquals(funcionario.getFuncao(),dao.postFuncionario(funcionario).getFuncao());
		assertEquals(funcionario.getCpf(),dao.postFuncionario(funcionario).getCpf());
		assertEquals(funcionario.getEmail(),dao.postFuncionario(funcionario).getEmail());	
		
	}
	
	@Test
	public void testGetFuncionarioById() {
		dao.findFuncionarioById(idFuncionario);
		String idFuncionario = funcionario.getId();
		Optional<Funcionario> procuraFuncionario = dao.findFuncionarioById(idFuncionario);
		if(procuraFuncionario.isPresent()) {
			Funcionario encontrado = procuraFuncionario.get();
			assertEquals(idFuncionario,encontrado.getId());
		}
	}
	
	@Test 
	public void testUpdateFuncionario() {
		dao.postFuncionario(funcionario);			
		Optional<Funcionario> procuraFuncionario = dao.findFuncionarioById(idFuncionario);
		if(procuraFuncionario.isPresent()) {
			Funcionario encontrado = procuraFuncionario.get();
			dao.updateFuncionario(encontrado.getId(), encontrado.getNome(), encontrado.getIdade(), encontrado.getFuncao(), encontrado.getEmail());
			assertEquals(funcionario.getId(),encontrado.getId());
			assertEquals(funcionario.getNome(),encontrado.getNome());
			assertEquals(funcionario.getIdade(),encontrado.getIdade());
			assertEquals(funcionario.getFuncao(),encontrado.getFuncao());
		}
	}
		
	@Test
	public void testDeleteFuncionario() {
		Optional<Funcionario> encontraFuncionario = dao.findFuncionarioById(idFuncionario);
		if(encontraFuncionario.isPresent()) {
			Funcionario encontrado = encontraFuncionario.get();
			dao.deleteFuncionario(idFuncionario);
			assertEquals(funcionario.getId(),encontrado.getId());
		}		
	}
}
