import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import grupo3aluguel.meiopagamento.MeioPagamento;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;

public class TesteMeioPagamento {
	MeioPagamentoDAO dao; 
	MeioPagamento meioPagamento = new MeioPagamento("Anna Bia", "12345678910", "25/09/2025", "123");
	String idMeioPagamento = meioPagamento.getId();
	
	@Before
	public void before() {
		dao = MeioPagamentoDAO.instance();
	}
	
	@After
	public void after() {
		dao.resetInstance();
	}
	
	@Test
	public void testGetId() {
		assertEquals(idMeioPagamento,meioPagamento.getId());
	}
	
	@Test
	public void testSetId() {
		meioPagamento.setId("12345");
		assertEquals("12345", meioPagamento.getId());
	}
	
	@Test
	public void testGetValidade() {
		assertEquals("25/09/2025",meioPagamento.getValidade());
	}
	
	@Test
	public void testSetValidade() {
		meioPagamento.setValidade("25/10/2025");
		assertEquals("25/10/2025",meioPagamento.getValidade());
	}
	
	@Test
	public void testGetNomeTitular() {
		assertEquals("Anna Bia",meioPagamento.getNomeTitular());
	}
	
	@Test
	public void testSetNomeTitular() {
		meioPagamento.setNomeTitular("Mario de Castro");
		assertEquals("Mario de Castro",meioPagamento.getNomeTitular());
	}
	
	@Test
	public void testGetCvv() {
		assertEquals("123",meioPagamento.getCvv());
	}
	
	@Test
	public void testSetCvv() {
		meioPagamento.setCvv("133");
		assertEquals("133",meioPagamento.getCvv());
	}
	
	@Test
	public void testGetNumero() {
		assertEquals("12345678910",meioPagamento.getNumero());
	}
	
	@Test
	public void testSetNumero() {
		meioPagamento.setNumero("12345658910");
		assertEquals("12345658910",meioPagamento.getNumero());
	}

	@Test
	public void testPostMeioPagamento() {		
		assertEquals(meioPagamento.getValidade(),dao.postMeioPagamento(meioPagamento).getValidade());
		assertEquals(meioPagamento.getNomeTitular(),dao.postMeioPagamento(meioPagamento).getNomeTitular());
		assertEquals(meioPagamento.getCvv(),dao.postMeioPagamento(meioPagamento).getCvv());
		assertEquals(meioPagamento.getNumero(),dao.postMeioPagamento(meioPagamento).getNumero());
		
	}
	
	@Test
	public void testGetMeioPagamentoById() {
		dao.findMeioPagamentoById(idMeioPagamento);
		String idMeioPagamento = meioPagamento.getId();
		Optional<MeioPagamento> procuraMeioPagamento = dao.findMeioPagamentoById(idMeioPagamento);
		if(procuraMeioPagamento.isPresent()) {
			MeioPagamento encontrado = procuraMeioPagamento.get();
			assertEquals(idMeioPagamento,encontrado.getId());
		}
	}
	
	@Test 
	public void testUpdateMeioPagamento() {
		dao.postMeioPagamento(meioPagamento);			
		Optional<MeioPagamento> procuraMeioPagamento = dao.findMeioPagamentoById(idMeioPagamento);
		if(procuraMeioPagamento.isPresent()) {
			MeioPagamento encontrado = procuraMeioPagamento.get();
			dao.updateMeioPagamento(encontrado.getId(), encontrado.getNomeTitular(), encontrado.getNumero(), encontrado.getValidade(), encontrado.getCvv());
			assertEquals(meioPagamento.getId(),encontrado.getId());
			assertEquals(meioPagamento.getNomeTitular(),encontrado.getNomeTitular());
			assertEquals(meioPagamento.getCvv(),encontrado.getCvv());
		}
	}
		
}
