import static org.junit.Assert.*;

import org.junit.Test;

import grupo3aluguel.Aluguel.Aluguel;
import grupo3aluguel.Aluguel.Aluguel.StatusAluguel;
import grupo3aluguel.Aluguel.Aluguel.StatusCobranca;

public class TesteAluguel {
	Aluguel aluguel = new Aluguel("a4fbb236-ffdc-405e-b73b-d2b7d42c6ad1", "18/05/2021 16:38:13", "7b7a8ef8-21a8-4a80-9515-318179f7374e");

//	@Before
//	public void before() {
//		dao = CiclistaDAO.instance();
//	}
//	
//	@After
//	public void after() {
//		dao.resetInstance();
//	}
	
	@Test
	public void testGetId() {
		System.out.println(aluguel.getId());
		assertNotNull(aluguel.getId());
	}
	
	@Test
	public void testSetId() {
		String idTeste = "123456";
		aluguel.setId(idTeste);
		assertEquals(idTeste,aluguel.getId());
	}
	
	@Test
	public void testGetCiclista() {
		String idCiclista = "a4fbb236-ffdc-405e-b73b-d2b7d42c6ad1";
		assertEquals(idCiclista,aluguel.getCiclista());
	}
	
	@Test
	public void testSetCiclista() {
		String ciclistaTeste = "Anna Beatriz";
		aluguel.setCiclista(ciclistaTeste);
		assertEquals(ciclistaTeste,aluguel.getCiclista());
	}
	
	@Test
	public void testGetBicicleta() {
		assertNull(aluguel.getBicicleta());
	}
	
	@Test
	public void testSetBicicleta() {
		String testeBicicleta = "78910";
		aluguel.setBicicleta(testeBicicleta);
		assertEquals(testeBicicleta,aluguel.getBicicleta());
	}
	
	@Test
	public void testGetHoraInicio() {
		String horarioInicio = "18/05/2021 16:38:13";
		assertEquals(horarioInicio,aluguel.getHoraInicio());
	}
	
	@Test
	public void testSetHoraInicio() {
		String testeHoraInicio = "12:45";
		aluguel.setHoraInicio(testeHoraInicio);
		assertEquals(testeHoraInicio,aluguel.getHoraInicio());
	}

	@Test
	public void testGetTrancaInicio() {
		String trancaInicio = "7b7a8ef8-21a8-4a80-9515-318179f7374e";
		assertEquals(trancaInicio,aluguel.getTrancaInicio());
	}
	
	@Test
	public void testSetTrancaInicio() {
		String testeTrancaInicio = "101112";
		aluguel.setTrancaInicio(testeTrancaInicio);
		assertEquals(testeTrancaInicio,aluguel.getTrancaInicio());
	}
	
	@Test
	public void testGetCobranca() {
		String cobrancaTeste = null;
		assertEquals(cobrancaTeste,aluguel.getCobranca());
	}
	
	
	@Test
	public void testSetCobranca() {
		String testeCobranca = "5";
		aluguel.setCobranca(testeCobranca);
		assertEquals(testeCobranca,aluguel.getCobranca());
	}
	
	@Test
	public void testGetHoraFim() {
		String horafimTeste = null;
		assertEquals(horafimTeste,aluguel.getHoraFim());
	}
	
	@Test
	public void testSetHoraFim() {
		String testeHoraFim = "13:45";
		aluguel.setHoraFim(testeHoraFim);
		assertEquals(testeHoraFim,aluguel.getHoraFim());
	}
	
	@Test
	public void testGetTrancaFim() {
		String trancaFimTeste = null;
		assertEquals(trancaFimTeste,aluguel.getTrancaFim());
	}
	
	@Test
	public void testSetTrancaFim() {
		String testeTrancaFim = "131415";
		aluguel.setTrancaFim(testeTrancaFim);
		assertEquals(testeTrancaFim,aluguel.getTrancaFim());
	}
	
	@Test
	public void testGetStatusCobranca() {
		StatusCobranca statusCobrancaTeste = StatusCobranca.FINALIZADO;
		aluguel.setStatusCobranca(statusCobrancaTeste);

		assertEquals(statusCobrancaTeste,aluguel.getStatusCobranca());		
	}
	
	@Test
	public void testSetStatusCobranca() {
		StatusCobranca testeStatusCobranca = StatusCobranca.PAGAMENTOPENDENTE;
		aluguel.setStatusCobranca(testeStatusCobranca);
		assertEquals(testeStatusCobranca,aluguel.getStatusCobranca());
	}
	
	@Test
	public void testGetStatusAluguel() {
		StatusAluguel statusAluguelTeste = StatusAluguel.ABERTO;
		assertEquals(statusAluguelTeste,aluguel.getStatusAluguel());		
	}
	
	@Test
	public void testSetStatusAluguel() {
		StatusAluguel testeStatusAluguel = StatusAluguel.FINALIZADO;
		aluguel.setStatusAluguel(testeStatusAluguel);
		assertEquals(testeStatusAluguel,aluguel.getStatusAluguel());
	}
	
	
}
