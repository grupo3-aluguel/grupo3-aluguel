import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.*;

import grupo3aluguel.ciclista.*;
import grupo3aluguel.ciclista.Ciclista.Nacionalidade;
import grupo3aluguel.meiopagamento.MeioPagamento;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;

public class TesteCiclistaDao {
	CiclistaDAO dao;
	MeioPagamentoDAO daoPagamento;
	MeioPagamento meioDePagamento = new MeioPagamento("Anna Bia", "12345678910", "25/09/2025", "123");
	Passaporte passaporte = new Passaporte("24681013579","10/03/1022","Italia");
	public MeioPagamento meioPagamento2 = new MeioPagamento("Anna Maria", "12345678910", "28/06/2021", "321");
	Ciclista ciclista = new Ciclista("Anna Bia", "05/01/1995", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.bia@gmail.com", meioDePagamento);
	Ciclista ciclista1 = new Ciclista("Anna Maria", "08/05/1986", passaporte, Nacionalidade.ESTRANGEIRO, "xxxxx", "anna.maria@gmail.com", meioPagamento2);
	ArrayList<Ciclista> ciclistas = new ArrayList<>();
	String idCiclista = ciclista.getId();
	String emailCiclista = ciclista.getEmail();
	
	@Before
	public void before() {
		dao = CiclistaDAO.instance();
		daoPagamento = MeioPagamentoDAO.instance();
	}
	
	@After
	public void after() {
		dao.resetInstance();
		daoPagamento.resetInstance();
	}

	@Test
	public void testPostCiclista() {		
		assertEquals(ciclista.getNome(),dao.postCiclista(ciclista).getNome());
		assertEquals(ciclista.getNascimento(),dao.postCiclista(ciclista).getNascimento());
		assertEquals(ciclista.getNacionalidade(),dao.postCiclista(ciclista).getNacionalidade());
		assertEquals(ciclista.getSenha(),dao.postCiclista(ciclista).getSenha());
		assertEquals(ciclista.getEmail(),dao.postCiclista(ciclista).getEmail());
		assertEquals(ciclista.getMeioDePagamento(),dao.postCiclista(ciclista).getMeioDePagamento());	
	}
	
	@Test
	public void testPostCiclistaEstrangeiro() {		
		assertEquals(ciclista1.getNome(),dao.postCiclista(ciclista1).getNome());
		assertEquals(ciclista1.getNascimento(),dao.postCiclista(ciclista1).getNascimento());
		assertEquals(ciclista1.getNacionalidade(),dao.postCiclista(ciclista1).getNacionalidade());
		assertEquals(ciclista1.getPassaporte(),dao.postCiclista(ciclista1).getPassaporte());
		assertEquals(ciclista1.getSenha(),dao.postCiclista(ciclista1).getSenha());
		assertEquals(ciclista1.getEmail(),dao.postCiclista(ciclista1).getEmail());
		assertEquals(ciclista1.getMeioDePagamento(),dao.postCiclista(ciclista1).getMeioDePagamento());	
	}
	
	@Test
	public void testPostMeioPagamento(){
		assertEquals(meioDePagamento.getNomeTitular(),daoPagamento.postMeioPagamento(meioDePagamento).getNomeTitular());
		assertEquals(meioDePagamento.getNumero(),daoPagamento.postMeioPagamento(meioDePagamento).getNumero());
		assertEquals(meioDePagamento.getValidade(),daoPagamento.postMeioPagamento(meioDePagamento).getValidade());
		assertEquals(meioDePagamento.getCvv(),daoPagamento.postMeioPagamento(meioDePagamento).getCvv());
	}
	
	@Test
	public void testGetCiclistaById() {
		dao.postCiclista(ciclista);
		String idCiclista = ciclista.getId();
		Optional<Ciclista> procuraCiclista = dao.findCiclistaById(idCiclista);
		if(procuraCiclista.isPresent()) {
			Ciclista encontrado = procuraCiclista.get();
			assertEquals(idCiclista,encontrado.getId());
		}
	}
	
	@Test 
	public void testUpdateCiclista() {
		Ciclista ciclista2 = new Ciclista("Anna Beatriz","20/04/1992","45780234846",Nacionalidade.BRASILEIRO,"xxxxx","annabeatriz@gmail.com",meioDePagamento);
		dao.postCiclista(ciclista2);			
		Optional<Ciclista> procuraCiclista = dao.findCiclistaById(idCiclista);
		
		if(procuraCiclista.isPresent()) {
			Ciclista encontrado = procuraCiclista.get();
			dao.updateCiclista(encontrado.getId(), "Anna Beatriz", "20/04/1992", "annabeatriz@gmail.com");
			assertEquals("Anna Beatriz",encontrado.getNome());
			assertEquals("20/04/1992",encontrado.getNascimento());
			assertEquals("annabeatriz@gmail.com",encontrado.getEmail());
		}
	}
	
	@Test
	public void testFindAllCiclistas() {
		ArrayList<Ciclista> encontraCiclistas = dao.findAllCiclistas();
	    assertTrue(encontraCiclistas.size() > 0);
	}
	
	@Test
	public void testAtivarCiclista() {
		dao.ativarCiclista(idCiclista);
		assertEquals(ciclista.getStatus(),dao.postCiclista(ciclista).getStatus());
	}
	
	@Test
	public void testVerificaExisteEmail() {		
		assertTrue(dao.verificaExisteEmail(emailCiclista));
	}

}
