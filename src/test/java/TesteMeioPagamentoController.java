import static org.junit.Assert.assertEquals;

import org.junit.Test;

import grupo3aluguel.Rotas;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

public class TesteMeioPagamentoController {
	 public MeioPagamentoDAO meioPagamentoDAO;
	 public Rotas app = new Rotas();
	 
	  @Test
	  public void getAllMeioPagamentosTest() {
		app.startApplication(5000);
		HttpResponse<String> responseponse = Unirest.get("http://localhost:5000/cartaoDeCredito").asString();
		assertEquals(200, responseponse.getStatus());
		app.stop();
	  }
	  
	  @Test
	  public void getMeioPagamentoById404Test() {
		  
		app.startApplication(5002);
			
		HttpResponse<JsonNode> responseponse = Unirest.get("http://localhost:5002/cartaoDeCredito/{idMeioPagamento}")
				.routeParam("idMeioPagamento", "1")
				.asJson();
		
		assertEquals(404, responseponse.getStatus());
				
		app.stop();
	  }


	  @Test
	  public void putMeioPagamentoInvalidTest() {
	    app.startApplication(5006);
		MeioPagamentoDAO dao = null;
	    String id = "2";
	    HttpResponse<String> response = Unirest.put("http://localhost:5006/cartaoDeCredito/{idMeioPagamento}")
	    		.routeParam("idMeioPagamento", id)
	    		.body(dao).asString();
	    
	    assertEquals(400, response.getStatus());
	    app.stop();
	  }
	  
}
