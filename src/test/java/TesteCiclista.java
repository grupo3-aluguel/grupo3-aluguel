import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;


import grupo3aluguel.ciclista.Ciclista;
import grupo3aluguel.ciclista.CiclistaDAO;
import grupo3aluguel.ciclista.Passaporte;
import grupo3aluguel.ciclista.Ciclista.Nacionalidade;
import grupo3aluguel.ciclista.Ciclista.Status;
import grupo3aluguel.meiopagamento.MeioPagamento;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;

public class TesteCiclista {
	CiclistaDAO dao;
	MeioPagamentoDAO daoPagamento;
	MeioPagamento meioDePagamento = new MeioPagamento("Anna Bia", "12345678910", "25/09/2025", "123");
	Passaporte passaporte = new Passaporte("24681013579","10/03/1022","It�lia");
	public MeioPagamento meioPagamento2 = new MeioPagamento("Anna Maria", "12345678910", "28/06/2021", "321");
	Ciclista ciclista = new Ciclista("Anna Bia", "05/01/1995", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.bia@gmail.com", meioDePagamento);
	Ciclista ciclista1 = new Ciclista("Anna Maria", "08/05/1986", passaporte, Nacionalidade.ESTRANGEIRO, "xxxxx", "anna.maria@gmail.com", meioPagamento2);
	ArrayList<Ciclista> ciclistas = new ArrayList<>();
	String idCiclista = ciclista.getId();
	String emailCiclista = ciclista.getEmail();
	
	@Before
	public void before() {
		dao = CiclistaDAO.instance();
		daoPagamento = MeioPagamentoDAO.instance();
	}
	
	@After
	public void after() {
		dao.resetInstance();
		daoPagamento.resetInstance();
	}
	
	@Test
	public void testGetId() {
		assertEquals(idCiclista,ciclista.getId());
	}
	
	@Test
	public void testGetStatus(){
		assertEquals(Status.AGUARDANDO_CONFIRMACAO,ciclista.getStatus());
	}
	
	@Test
	public void testGetNome() {
		assertEquals("Anna Bia",ciclista.getNome());
	}
	
	@Test
	public void testGetNascimento() {
		assertEquals("05/01/1995",ciclista.getNascimento());
	}
	
	@Test
	public void testGetCpf() {
		assertEquals("12345678910",ciclista.getCpf());
	}
	
	@Test
	public void testGetPassaporte() {
		assertEquals(passaporte,ciclista1.getPassaporte());
	}
	
	@Test
	public void testGetNacionalidade() {
		assertEquals(Nacionalidade.BRASILEIRO,ciclista.getNacionalidade());
	}
	
	@Test
	public void testGetNacionalidade2() {
		assertEquals(Nacionalidade.ESTRANGEIRO,ciclista1.getNacionalidade());
	}
	
	@Test 
	public void testGetSenha() {
		assertEquals("xxxxx",ciclista.getSenha());
	}
	
	@Test
	public void testGetEmail() {
		assertEquals("anna.bia@gmail.com",ciclista.getEmail());
	}
	
	@Test
	public void testGetMeioDePagamento() {
		assertEquals(meioDePagamento,ciclista.getMeioDePagamento());
	}
	
	@Test
	public void testSetStatus() {
		ciclista.setStatus(Status.ATIVO);
		assertEquals(Status.ATIVO,ciclista.getStatus());
	}
	
	@Test
	public void testSetNome() {
		ciclista.setNome("Anna Beatriz");
		assertEquals("Anna Beatriz",ciclista.getNome());
	}
	
	@Test
	public void testSetNascimento() {
		ciclista.setNascimento("03/05/1995");
		assertEquals("03/05/1995",ciclista.getNascimento());
	}
	
	@Test
	public void testSetCpf() {
		ciclista.setCpf("01987654321");
		assertEquals("01987654321",ciclista.getCpf());
	}
	
	@Test
	public void testSetPassaporte() {
		Passaporte passaporte1 = new Passaporte("13579246810","15/09/2026","Fran�a");
		ciclista1.setPassaporte(passaporte1);
		assertEquals(passaporte1,ciclista1.getPassaporte());
	}
	
	@Test
	public void testeSetNacionalidade() {
		ciclista.setNacionalidade(Nacionalidade.ESTRANGEIRO);
		assertEquals(Nacionalidade.ESTRANGEIRO,ciclista.getNacionalidade());
	}
	
	@Test
	public void testSetSenha() {
		ciclista.setSenha("yyyyy");
		assertEquals("yyyyy",ciclista.getSenha());
	}
	
	@Test
	public void testSetEmail() {
		ciclista.setEmail("annabeatriz@gmail.com");
		assertEquals("annabeatriz@gmail.com",ciclista.getEmail());
	}
	
	@Test
	public void testSetMeioDePagamento() {
		ciclista.setMeioDePagamento(meioPagamento2);
		assertEquals(meioPagamento2,ciclista.getMeioDePagamento());
	}

}
