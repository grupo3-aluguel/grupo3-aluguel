import static org.junit.Assert.*;

import java.util.Optional;

import org.junit.Test;

import grupo3aluguel.Rotas;
import grupo3aluguel.ciclista.Ciclista;
import grupo3aluguel.ciclista.CiclistaDAO;
import grupo3aluguel.ciclista.Ciclista.Nacionalidade;
import grupo3aluguel.meiopagamento.MeioPagamento;
import grupo3aluguel.meiopagamento.MeioPagamentoDAO;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.*;

public class TesteCiclistaController {
	 public CiclistaDAO ciclistaDAO;
	 public Rotas app = new Rotas();
	 
	 MeioPagamento meioPagamento = new MeioPagamento("Anna Bia", "12345678910", "25/09/2025", "123");
	 Ciclista ciclista = new Ciclista("1", "Carlos", "05/01/1995", "45678912345", Nacionalidade.BRASILEIRO, "xxxx", "Carlos@gmail.com", meioPagamento);
	 
	  @Test
	  public void getAllCiclistasTest() {
		app.startApplication(8000);
		HttpResponse<String> responseponse = Unirest.get("http://localhost:8000/ciclista").asString();
		assertEquals(200, responseponse.getStatus());
		app.stop();
	  }
	  
	  @Test
	  public void getCiclistaByIdTest() {
		app.startApplication(8001);
		CiclistaDAO dao = CiclistaDAO.instance();
		Optional<Ciclista> ciclistaExists = dao.findCiclistaByNome("Mariana");
		
		if (ciclistaExists.isPresent()) {
			Ciclista ciclista = ciclistaExists.get();
			HttpResponse<String> responseponse = Unirest.get("http://localhost:8001/ciclista/" + ciclista.getId()).asString();
			assertEquals(200, responseponse.getStatus());
		}
		
		dao.resetInstance();
		app.stop();
	  }
	  
	  @Test
	  public void getCiclistaById404Test() {
		  
		app.startApplication(8002);
			
		HttpResponse<JsonNode> responseponse = Unirest.get("http://localhost:8002/ciclista/{idCiclista}")
				.routeParam("idCiclista", "1")
				.asJson();
		
		assertEquals(404, responseponse.getStatus());
				
		app.stop();
	  }
	  
	  @Test
	  public void postCiclistaTest() {
	    app.startApplication(8003);
		Ciclista ciclista = new Ciclista("teste", "05/01/1995", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.bia@gmail.com", MeioPagamentoDAO.instance().meioPagamento);
	    String ciclistaToJson = JavalinJson.toJson(ciclista);
	    
	    HttpResponse<String> response = Unirest.post("http://localhost:8003/ciclista").body(ciclistaToJson).asString();
	    assertEquals(201, response.getStatus());
	    app.stop();
	  }

	  @Test
	  public void post400CiclistaTest() {
	    app.startApplication(8004);
		Ciclista ciclista = null;
	    HttpResponse<String> response = Unirest.post("http://localhost:8004/ciclista").body(ciclista).asString();
	    assertEquals(400, response.getStatus());
	    app.stop();
	  }
	  
	  
	  @Test
	  public void putCiclistaTest() {
	    app.startApplication(8005);
	    
		CiclistaDAO dao = CiclistaDAO.instance();
		Ciclista ciclista = new Ciclista("teste", "05/01/1995", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.bia@gmail.com", MeioPagamentoDAO.instance().meioPagamento);
		dao.postCiclista(ciclista);
		
	    Optional<Ciclista> ciclistaExist = dao.findCiclistaByNome("teste");
	    
	    if (ciclistaExist.isPresent()) {
	      Ciclista ciclistaToUpdate = ciclistaExist.get();
	      String id = ciclistaToUpdate.getId();
	      String ciclistaToJson = JavalinJson.toJson(ciclistaToUpdate);
	      
	      HttpResponse<String> response = Unirest.put("http://localhost:8005/ciclista/{idCiclista}")
	    		  .routeParam("idCiclista", id)
	    		  .body(ciclistaToJson).asString();
	      
	      assertEquals(200, response.getStatus());
	    }
	    
	    dao.resetInstance();
	    app.stop();
	  }

	  @Test
	  public void putCiclistaInvalidTest() {
	    app.startApplication(8006);
		CiclistaDAO dao = null;
	    String id = "2";
	    HttpResponse<String> response = Unirest.put("http://localhost:8006/ciclista/{idCiclista}")
	    		.routeParam("idCiclista", id)
	    		.body(dao).asString();
	    
	    assertEquals(400, response.getStatus());
	    app.stop();
	  }

	  @Test(expected = IllegalArgumentException.class)
	  public void putCiclistaNullTest() {
	    app.startApplication(8007);
	    
		CiclistaDAO dao = CiclistaDAO.instance();
		Ciclista ciclista = new Ciclista("teste", "05/01/1995", "12345678910", Nacionalidade.BRASILEIRO, "xxxxx", "anna.bia@gmail.com", MeioPagamentoDAO.instance().meioPagamento);
		dao.postCiclista(ciclista);
		
	    Optional<Ciclista> ciclistaExists = dao.findCiclistaByNome("teste");
	    if (ciclistaExists.isPresent()) {
	      Ciclista ciclistaToUpdate = ciclistaExists.get();
	      String id = ciclistaToUpdate.getId();
	      Ciclista ciclistaToInsert = null;
	      
	      String ciclistaToJson = JavalinJson.toJson(ciclistaToInsert);

	      HttpResponse<String> response = Unirest.put("http://localhost:8007/ciclista/{idCiclista}")
	    		  .routeParam("idCiclista", id)
	    		  .body(ciclistaToJson).asString();
	      
	      assertEquals(404, response.getStatus());
	    }
	    
	    dao.resetInstance();
	    app.stop();
	  }
	  
	  @Test
	  public void UpdateStatusCiclistaTest() {
		  app.startApplication(8008);
		    
			CiclistaDAO dao = CiclistaDAO.instance();
			
		    Optional<Ciclista> ciclistaExists = dao.findCiclistaByNome("Mariana");
		    
		    if (ciclistaExists.isPresent()) {
			      Ciclista ciclista = ciclistaExists.get();
			      String id = ciclista.getId();
	     
		      HttpResponse<String> res = Unirest
		    		  .put("http://localhost:8008/ciclista/{idCiclista}/ativar")
		    		  .routeParam("idCiclista", id)
		    		  .asString();
		      
		      assertEquals(200, res.getStatus());
	    }
		    
		    app.stop();
	  }
}
